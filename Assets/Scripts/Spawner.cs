using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Spawner : MonoBehaviour
{
    private int waveNumber = 0;
    public int enemySpawnAmount = 0;
    public int enemiesKilled = 0;

    public GameObject[] spawners;
    public GameObject zombie;

    public GameManager man;

    public GameController controller;

    public CharacterStats zom;

    private void Start()
    {
        spawners = new GameObject[52];

        for(int i = 0; i < spawners.Length; i++)
        {
            spawners[i] = transform.GetChild(i).gameObject;
        }

        StartWave();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            SpawnEnemy();
        }

        if(enemiesKilled >= enemySpawnAmount)
        {
            NextWave();
        }
    }

    private void SpawnEnemy()
    {
        int spawnerID = Random.Range(0, spawners.Length);
        Instantiate(zombie, spawners[spawnerID].transform.position, spawners[spawnerID].transform.rotation);
    }

    private void StartWave()
    {
        waveNumber = 1;
        enemySpawnAmount = 5;
        enemiesKilled = 0;

        zombie.GetComponent<NavMeshAgent>().speed = 3;
        zom.maxHealth = 100;

        for (int i = 0;i < enemySpawnAmount; i++)
        {
            SpawnEnemy();
        }

        controller.UpdateMancheScore(waveNumber);
    }

    public void NextWave()
    {
        waveNumber++;
        enemySpawnAmount += 5;
        enemiesKilled = 0;

        zombie.GetComponent<NavMeshAgent>().speed += 3;
        zom.maxHealth += 30;

        for (int i = 0; i < enemySpawnAmount; i++)
        {
            SpawnEnemy();
        }

        controller.UpdateMancheScore(waveNumber);
    }
}
