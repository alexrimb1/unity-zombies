using UnityEngine;

public class CharacterStats : MonoBehaviour
{
    public float currHealth;
    public float maxHealth;

    public virtual void CheckHealth()
    {
        if(currHealth >= maxHealth) 
        {
            currHealth = maxHealth;
        }

        if(currHealth <= 0)
        {
            currHealth = 0;
            Die();
        }
    }

    public virtual void Die()
    {
        //Override
    }

    public void TakeDamage(float damage)
    {
        currHealth -= damage;
        CheckHealth();
    }
}
