using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    float currScore = 0;

    [SerializeField]
    PlayerWeapon weapon;

    [SerializeField]
    Text scoreAmount;

    [SerializeField]
    Text mancheScore;

    [SerializeField]
    Text currentAmmo;

    [SerializeField]
    Text maxAmmo;

    [SerializeField]
    Text realoding;
     
    private void Start()
    {
        currScore = 0;
        UpdateScoreUI();
        UpdateMancheScore(0f);
        maxAmmo.text = weapon.maxAmmo.ToString("0");
        currentAmmo.text = weapon.maxAmmo.ToString("0");
        realoding.text = "";
    }

    public void AddScore(float amount)
    {
        currScore += amount;
        UpdateScoreUI();
    }

    private void UpdateScoreUI()
    {
        scoreAmount.text = currScore.ToString("0");
    }

    public void UpdateMancheScore(float currManche)
    {
        mancheScore.text = currManche.ToString("0");
    }

    public void UpdateCurrAmmo(int currAmmo)
    {
        currentAmmo.text = currAmmo.ToString("0");
    }

    public void UpdateRealoding(string reload)
    {
        realoding.text = reload;
    }
}
