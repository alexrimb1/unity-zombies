using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private const string zombieIdPrefix = "Zombie";

    private static Dictionary<string, Zombie> zombies = new Dictionary<string, Zombie>();

    public void RegisterZombie(string netID, Zombie zombie)
    {
        string zombieId = zombieIdPrefix + netID;
        zombies.Add(zombieId, zombie);
        zombie.transform.name = zombieId;
    }

    public static void UnregisterZombie(string zombieId)
    {
        zombies.Remove(zombieId);
    }

    private void OnGUI()
    {
        
    }

    public static Zombie GetZombie(string zombieID)
    {
        return zombies[zombieID];
    }
}
