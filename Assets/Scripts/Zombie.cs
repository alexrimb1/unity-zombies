using UnityEngine;

public class Zombie : MonoBehaviour
{
    [SerializeField]
    private float maxHealth = 100f;

    private float currentHealth;

    public void TakeDamage(float amount)
    {
        currentHealth -= amount;
    }
}
