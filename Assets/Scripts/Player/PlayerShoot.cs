using System.Collections;
using UnityEngine;


public class PlayerShoot : MonoBehaviour
{
    public PlayerWeapon weapon;

    [SerializeField]
    GameController controller;

    [SerializeField]
    private Camera cam;

    [SerializeField]
    private LayerMask mask;

    [SerializeField]
    int currAmmo;
    float reloadSpeed = 2;

    public new AudioSource audio;
    public new AudioClip audioShoot;
    public new AudioClip audioReload;

    public Transform bulletSpawnPoint;
    public GameObject bulletPrefab;
    public float bulletSpeed = 10;

    void Start()
    {
        if(cam == null)
        {
            Debug.LogError("Pas de camera renseign�e sur le syst�me de tir");
            this.enabled = false;
        }

        currAmmo = weapon.maxAmmo;
        audio = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            StartCoroutine(Reload());
            return;
        }

        if(Input.GetButtonDown("Fire1"))
        {
            if (currAmmo > 0)
            {
                Shoot();
            }
            else
            {
                controller.UpdateRealoding("Need reloading, press R");
            }
        }
    }

    private void Shoot()
    {
        currAmmo--;

        var bullet = Instantiate(bulletPrefab, bulletSpawnPoint.position, bulletSpawnPoint.rotation);
        bullet.GetComponent<Rigidbody>().velocity = bulletSpawnPoint.forward * bulletSpeed;

        controller.UpdateCurrAmmo(currAmmo);

        RaycastHit hit;
        audio.clip = audioShoot;
        audio.PlayOneShot(audio.clip);

        if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, weapon.range, mask))
        {
            EnemyStats enemyStats = hit.transform.GetComponent<EnemyStats>();
            if (enemyStats != null)
            {
                enemyStats.TakeDamage(weapon.damage);
            }
        }
    }

    IEnumerator Reload()
    {
        controller.UpdateRealoding("Reloading...");
        audio.clip = audioReload;
        audio.Play();
        yield return new WaitForSeconds(reloadSpeed);
        currAmmo = weapon.maxAmmo;
        controller.UpdateCurrAmmo(currAmmo);
        controller.UpdateRealoding("");
    }
}
