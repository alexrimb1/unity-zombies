[System.Serializable]
public class PlayerWeapon
{
    public string name = "Pistolet";
    public float damage = 10f;
    public float range = 100f;
    public int maxAmmo = 10;
}
