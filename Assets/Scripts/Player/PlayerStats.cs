using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerStats : CharacterStats
{
    PlayerUI playerUI;

    private void Start()
    {
        playerUI = GetComponent<PlayerUI>();

        maxHealth = 100;
        currHealth = maxHealth;

        SetStats();
    }

    public override void Die()
    {
        SceneManager.LoadScene(2);
        Cursor.lockState = CursorLockMode.None;
    }

    void SetStats()
    {
        playerUI.healthAmount.text = currHealth.ToString();
    }

    public override void CheckHealth()
    {
        base.CheckHealth();
        SetStats();
    }
}
